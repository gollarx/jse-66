package ru.t1.shipilov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.shipilov.tm.component.Bootstrap;
import ru.t1.shipilov.tm.configuration.ServerConfiguration;

public final class Application {

    public static void main(@Nullable final String... args) {
        @NotNull ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start();
    }

}
