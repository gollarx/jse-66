package ru.t1.shipilov.tm.exception.entity.field;

public class TaskIdEmptyException extends AbstractFieldException {

    public TaskIdEmptyException() {
        super("Error! Task id is incorrect...");
    }

}
